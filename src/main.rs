//! Web application to showcase personal projects and portfolio

// Dependencies

#[macro_use]
extern crate actix_web;

#[macro_use]
extern crate serde;

#[macro_use]
extern crate serde_json;

extern crate actix;
extern crate actix_http;
extern crate actix_rt;
extern crate handlebars;

// Modules

pub mod controllers;
pub mod models;
pub mod views;

// Imports

use {
    actix_web::{
        web,
        HttpServer,
        App,
    },
    handlebars::Handlebars,
    std::io,
};

#[actix_rt::main]
async fn main() -> io::Result<()> {
    let mut hb = Handlebars::new();
    hb.register_templates_directory(".html", "./wwwroot").unwrap();

    let hb_ref = web::Data::new(hb);

    return HttpServer::new(move || {
        App::new()
            .app_data(hb_ref.clone())
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await;
}
