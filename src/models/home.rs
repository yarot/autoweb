//! Implements a model for "home" pages

#[derive(Deserialize, Serialize)]
pub struct HomeModel {
    title: String,
    description: String,
}
